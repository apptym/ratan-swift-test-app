# README #

This is solution to interview problem stated in https://github.com/surya-soft/Interview/blob/master/iOS.md

# LIBRARIES USED #

HANEKE SWIFT: for caching of data and images.
[https://github.com/Haneke/HanekeSwift](Link URL)

SWIFTYJSON: SwiftyJSON makes it easy to deal with JSON data in Swift 
[https://github.com/SwiftyJSON/SwiftyJSON](Link URL)